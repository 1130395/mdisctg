package mdisc.MODEL;

/**
 * O Algoritmo dado pelos professores no enunciado de MDISC. Inicialmente irá se
 * gerar uma sequência de número de elementos n (dado pelo utilizador) e depois
 * descobrir a sequência máxima, somá-la e apresentar o resultado para o ecrã
 *
 * @author Grupo de MDISC
 */
public class Algoritmo2 {

    /**
     * Número de elementos da sequência Este valor é introduzido pelo utilizador
     * e passado por parâmetro
     */
    int n;

    /**
     * seq - Sequência de n elementos aleatórios SeqMax - Sequência onde a soma
     * de elementos seguidos é máxima
     */
    int[] seq, SeqMax;
    int max_ateagora = 0;

    

    /**
     * CONSTRUTOR
     *
     * @param n número de elementos da sequência. Este constutor irá inicializar
     * a sequência como um vetor de n elementos. Irá gerar depois uma sequência
     * aleatória. Finalmente, irá dizer ao utilizador qual a sequência máxima
     * (ou seja, qual a soma de elementos seguidos que é máxima e mostrar e
     * mostrar essa sequência) e a soma máxima.
     */
    public Algoritmo2(int n) {
        this.n = n;
        seq = new int[n];
        gerarSeq(n);
        MaxSubSeq2(seq);
    }

    /**
     * METODO GERAR SEQUÊNCIA
     *
     * @param n número de elementos da sequência. Este método gerarSeq(int n)
     * irá gerar uma sequência aleatória de n elementos onde esses elementos se
     * encontram num intervalo de [-n, n]. Para esse propósito, utilizamos a
     * class Math e o método random().
     */
    private void gerarSeq(int n) {
        seq = new int[n];
        for (int i = 0; i < n; i++) {
            seq[i] = -n + (int) ((Math.random()) * ((n - (-n)) + 1));
        }
    }

    /**
     * METODO MAXIMA SUBSEQUÊNCIA DA SEQUÊNCIA PRINCIPAL
     *
     * @param seq um vetor de n elementos que constitui a sequência principal à
     * qual se vai querer saber qual a sua subsequência máxima onde a soma dos
     * seus elementos (seguidos) é máxima. Este método MaxSubSeq2(int [] seq)
     * foi disponibilizado no nosso enunciado de MDISC.
     */
    public void MaxSubSeq2(int[] seq) {
        int i_max_ateagora = 0;
        int f_max_ateagora = 0;
        int i_max_acabaaqui = 0;
        int f_max_acabaaqui = 0;
        int max_acabaaqui = 0;
        for (int i = 0; i <= seq.length-1; i++) {
            int s = seq[i];
            f_max_acabaaqui = i+1;
            if (max_acabaaqui + s < 0) {
                max_acabaaqui = 0;
                i_max_acabaaqui =i+1;
            } else {
                max_acabaaqui +=  s;
            }
            if (max_ateagora < max_acabaaqui) {
                max_ateagora = max_acabaaqui;
                i_max_ateagora = i_max_acabaaqui;
                f_max_ateagora = f_max_acabaaqui;
            }
        }
        SeqMax=new int[f_max_ateagora - i_max_ateagora ];
        System.arraycopy(seq, i_max_ateagora, SeqMax, 0, f_max_ateagora - i_max_ateagora );
    }


    /**
     * METODO GET SEQUÊNCIA MÁXIMA
     *
     * Este metodo serve para vir buscar a subsequência máxima
     *
     * @return seq
     */
    public int[] getSeqMax() {
        return SeqMax;
    }

    /**
     * METODO GET SEQUÊNCIA
     *
     * Este metodo serve para vir buscar a sequência principal
     *
     * @return seq
     */
    public int[] getSeq() {
        return seq;
    }

    /**
     * METODO GET MÁXIMO
     *
     * Este metodo serve para vir buscar a subsequência máxima
     *
     * @return max_ateagora
     */
    public int getMax_ateagora() {
        return max_ateagora;
    }

}
