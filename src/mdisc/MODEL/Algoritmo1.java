/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdisc.MODEL;

import java.util.Random;

/**
 * O Algoritmo pedido pelo corpo docente. Inicialmente irá se
 * gerar uma sequência de número de elementos n (dado pelo utilizador) e depois
 * descobrir a sequência máxima (á força bruta), somá-la e apresentar o resultado para o ecrã
 *
 * @author Grupo de MDISC
 */
public class Algoritmo1 {

    /**
     * Número de elementos da sequência Este valor é introduzido pelo utilizador
     * e passado por parâmetro
     */
    int n;
    int max = 0, tempMax = 0, limiteMin = 0, limiteMax = 0, cont = 0;
    Random rn = new Random();
    
     /**
     * seq - Sequência de n elementos aleatórios SeqMax - Sequência onde a soma
     * de elementos seguidos é máxima tempSeq - sequência temporária para o cálculo
     *da soma de elementos seguidos 
     */
    int[] seq, seqMax, tempSeq;

    /**
     * CONSTRUTOR
     *
     * @param n número de elementos da sequência. Este constutor irá inicializar
     * a sequência como um vetor de n elementos. Irá gerar depois uma sequência
     * aleatória. Finalmente, irá dizer ao utilizador qual a sequência máxima
     * (ou seja, qual a soma de elementos seguidos que é máxima e mostrar e
     * mostrar essa sequência) e a soma máxima.
     */
    public Algoritmo1(int n) {

        this.n = n;
        seq = new int[n];
        seqMax = new int[n];
        gerarSeq(n);
        long ti=System.nanoTime();
        gerarSeqMax(seq);
        long tf=System.nanoTime();
        long t=tf-ti;
        System.out.println(n+" "+t);

    }

    /**
     * METODO GERAR SEQUÊNCIA
     *
     * @param n número de elementos da sequência. Este método gerarSeq(int n)
     * irá gerar uma sequência aleatória de n elementos onde esses elementos se
     * encontram num intervalo de [-n, n]. Para esse propósito, utilizamos a
     * class Math e o método random().
     */
    private void gerarSeq(int n) {
        for (int i = 0; i < n; i++) {

            seq[i] = -n + (int) ((Math.random()) * ((n - (-n)) + 1));

        }

    }

    /**
     * METODO MAXIMA SUBSEQUÊNCIA DA SEQUÊNCIA PRINCIPAL
     *
     * @param seq um vetor de n elementos que constitui a sequência principal à
     * qual se vai querer saber qual a sua subsequência máxima onde a soma dos
     * seus elementos (seguidos) é máxima.
     */
    private void gerarSeqMax(int[] seq) { 

        for (int i = 0; i < seq.length; i++) {
            tempMax = 0;
            for (int j = i; j < seq.length; j++) {

                tempMax += seq[j];
                if (tempMax > max) {

                    limiteMin = i;
                    limiteMax = j;
                    max = tempMax;

                }
            }          

        }
        if(max<=0){
            seqMax= new int[0];
        }else{
        seqMax=new int[limiteMax - limiteMin + 1];
        System.arraycopy(seq, limiteMin, seqMax, 0, limiteMax - limiteMin + 1);
        }

    }

    /**
     * METODO GET SEQUÊNCIA MÁXIMA
     *
     * Este metodo serve para vir buscar a subsequência máxima
     *
     * @return seq
     */
    public int getMax() {
        return max;
    }

    /**
     * METODO GET SEQUÊNCIA
     *
     * Este metodo serve para vir buscar a sequência principal
     *
     * @return seq
     */
    public int[] getSeq() {
        return seq;
    }

    /**
     * METODO GET MÁXIMO
     *
     * Este metodo serve para vir buscar a subsequência máxima
     *
     * @return max_ateagora
     */
    public int[] getSeqMax() {
        return seqMax;
    }
}
