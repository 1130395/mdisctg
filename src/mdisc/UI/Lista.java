/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mdisc.UI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Jecabeda
 */
public class Lista extends JDialog {
    
    JTextField txtN;
     private static final Dimension LABEL_TAMANHO = new JLabel("SubSequência de maior soma:").getPreferredSize();
    
    public Lista(Frame pai) {
        super(pai, "Criar Lista", true);
        
        JPanel p1 = criarPainelCentro();
        JPanel p2 = criarPainelBotoes();        
        
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        
     
        setLocationRelativeTo(null);
        setSize(100, 100);
        pack();
        setResizable(true);
        setVisible(true);
        
    }

    private JPanel criarPainelBotoes() {
        
        JPanel p= new JPanel();
        p.add(criarBotaoInserir());
        p.add(criarBotaoCancelar());
        
        
        return p;
          
    }

    private JButton criarBotaoInserir() {
        JButton btn = new JButton("Inserir");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               
                    String nome = txtN.getText();
                    
            }
        });
        return btn;
    }
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               dispose();
            }
        });
        return btn;
    }

    private JPanel criarPainelCentro() {
        JPanel p = new JPanel(new GridLayout(4,1));
        JPanel p1 = criarPainelN();
        JPanel p2 = criarPainelSeq();
        JPanel p3 = criarPainelSeqMax();
        JPanel p4 = criarPainelMax();
        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);

        return p;
    }

    private JPanel criarPainelN() {
        JLabel lbl = new JLabel("Tamanho da sequência:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtN = new JTextField(CAMPO_LARGURA);
        txtN.requestFocus();

        JPanel p = new JPanel(new FlowLayout());

        p.add(lbl);
        p.add(txtN);

        return p;
    }

    private JPanel criarPainelSeq() {
      JLabel lbl = new JLabel("Sequência Aleatória: ", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
//        txtnPecasT2 = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
//        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
//        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
//        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
//                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
//        p.add(txtnPecasT2);

        return p;
    }
//     private JPanel criarPainelLista() {
//        JPanel p = new JPanel();
//         Algoritmo1 alg = new Algoritmo1(Integer.parseInt(txtN.getText()),ListaAleatoria.this);
//        String x = "Sequência de tamanho"+txtN.getText()+": "+alg.getSeq();
//        int[] t = alg.getSeq();
//        JList l = new JList(Arrays.asList(t));
//        l.setVisibleRowCount(10);
//        final int LISTA_LARGURA = 300, LISTA_ALTURA = 100; 
//        l.setPreferredSize(new Dimension(LISTA_LARGURA, LISTA_ALTURA));
//        JScrollPane j = new JScrollPane(l);       
//        
//        p.add(j);
//
//        return p;
//    }

    private JPanel criarPainelSeqMax() {
         JLabel lbl = new JLabel("SubSequência de maior soma:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
//        txtnPecasT2 = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
//        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
//        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
//        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
//                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
//        p.add(txtnPecasT2);

        return p;
    }

    private JPanel criarPainelMax() {
       JLabel lbl = new JLabel("SubSequência de maior soma:", JLabel.RIGHT);
       
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
//        txtnPecasT2 = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
//        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
//        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
//        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
//                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
//        p.add(txtnPecasT2);

        return p;
    }

    
}