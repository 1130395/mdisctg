/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdisc.UI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import mdisc.MODEL.Algoritmo2;

/**
 *
 * @author Jecabeda
 */
public class ListaAleatoria2 extends JDialog {

    JTextField txtN;
    String SMax, SSeq, SSeqMax;
    private static final Dimension LABEL_TAMANHO = new JLabel("SubSequência de maior soma:").getPreferredSize();
    private final JLabel lblMax = new JLabel(SMax, JLabel.CENTER);
    private final JLabel lblSeq = new JLabel(SSeq, JLabel.CENTER);
    private final JLabel lblSeqMax = new JLabel(SSeqMax, JLabel.CENTER);

    public ListaAleatoria2(Frame pai) {
        super(pai, "Lista", true);

        JPanel p1 = criarPainelCentro();
        JPanel p2 = criarPainelBotoes();

        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);
        pack();
        setLocationRelativeTo(null);

        setResizable(true);
        setVisible(true);

    }

    private JPanel criarPainelBotoes() {

        JPanel p = new JPanel();
        p.add(criarBotaoInserir());
        p.add(criarBotaoCancelar());

        return p;

    }

    private JButton criarBotaoInserir() {
        JButton btn = new JButton("Inserir");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String N = txtN.getText();
                Algoritmo2 alg2 = new Algoritmo2(Integer.parseInt(N));
                SMax= Integer.toString(alg2.getMax_ateagora());
                lblMax.setText("Soma: "+SMax);
                SSeq = Arrays.toString(alg2.getSeq());
                lblSeq.setText("Sequência: "+SSeq);
                SSeqMax = Arrays.toString(alg2.getSeqMax());
                lblSeqMax.setText("Subsequência: "+SSeqMax);

            }
        });
        return btn;
    }
    
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    private JPanel criarPainelCentro() {
        JPanel p = new JPanel(new GridLayout(4, 1));
        JPanel p1 = criarPainelN();
        JPanel p2 = criarPainelSeq();
        JPanel p3 = criarPainelSeqMax();
        JPanel p4 = criarPainelMax();
        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);

        return p;
    }

    private JPanel criarPainelN() {
        JLabel lbl = new JLabel("Tamanho da sequência:", JLabel.RIGHT);
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtN = new JTextField(CAMPO_LARGURA);
        txtN.requestFocus();

        JPanel p = new JPanel(new FlowLayout());

        p.add(lbl);
        p.add(txtN);

        return p;
    }

    private JPanel criarPainelSeq() {
   
        lblSeq.setPreferredSize(LABEL_TAMANHO);

         JPanel p = new JPanel(new BorderLayout());

        p.add(lblSeq);

        return p;
    }

    private JPanel criarPainelSeqMax() {
        lblSeqMax.setPreferredSize(LABEL_TAMANHO);

         JPanel p = new JPanel(new BorderLayout());

        p.add(lblSeqMax);

        return p;
    }

    private JPanel criarPainelMax() {
        
         lblMax.setPreferredSize(LABEL_TAMANHO);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.CENTER));

        p.add(lblMax);

        return p;
    }

}
