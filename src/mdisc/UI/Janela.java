package mdisc.UI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 *
 * @author Cristina
 */
public class Janela extends JFrame {

    public Janela() {

        super("Sequência de números inteiros");

        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        menuBar.add(criarMenuAlgoritmo1());
        menuBar.add(criarMenuAlgoritmo2());
        menuBar.add(criarMenuAjuda());

        add(new JLabel(new ImageIcon("isep.jpg")));

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                fechar();
            }
        });
        
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setResizable(true);
        setVisible(true);
        
    }
    
    private void fechar() {
        String[] opSimNao = {"Sim", "Não"};
        int resposta = JOptionPane.showOptionDialog(this,
                "Deseja fechar a aplicação?",
                "Gestão de Contas Bancárias",
                0,
                JOptionPane.QUESTION_MESSAGE,
                null,
                opSimNao,
                opSimNao[1]);

        final int SIM = 0;
        if (resposta == SIM) {
            dispose();
        }
    }

    private JMenu criarMenuAjuda() {
        JMenu menu = new JMenu("Ajuda");
        menu.setMnemonic(KeyEvent.VK_A);
        menu.add(criarItemAcerca());
        menu.addSeparator();
        menu.add(criarItemSair());

        return menu;
    }

//    private JMenuItem criarItemN() {
//        JMenuItem item = new JMenuItem("Inserir Comprimento da Lista", KeyEvent.VK_C);
//        return item;
//
//    }
//
//    private JMenuItem criarItemQuantidadeListas() {
//        JMenuItem item = new JMenuItem("Quantidade de Listas", KeyEvent.VK_Q);
//        return item;
//
//    }

    private JMenuItem criarItemSair() {
        JMenuItem item = new JMenuItem("Sair", KeyEvent.VK_S);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fechar();
            }
        });
        return item;
    }

//    private JMenuItem criarItemInserirNumeros() {
//        JMenuItem item = new JMenuItem("Inserir Números", KeyEvent.VK_I);
//        return item;
//    }

    private JMenuItem criarItemAcerca() {
        JMenuItem item = new JMenuItem("Acerca", KeyEvent.VK_A);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(rootPane, "MDISC 2013/2014 \nTrabalho realizado por \n- Cristina Lopes 1130371\n- Eduardo da Rocha 1130480\n- José Cabeda 1130395", "Acerca", JOptionPane.INFORMATION_MESSAGE);
            
            }
        });
        return item;
    }

    private JMenu criarMenuAlgoritmo1() {
               JMenu menu = new JMenu("Algoritmo 1");
        menu.setMnemonic(KeyEvent.VK_1);
        menu.add(criarItemGerarListaAleatória1());
//        menu.add(criarItemInserirLista());


        return menu;
    }

    private JMenu criarMenuAlgoritmo2() {
                JMenu menu = new JMenu("Algoritmo 2");
        menu.setMnemonic(KeyEvent.VK_2);
        menu.add(criarItemGerarListaAleatória2());
//        menu.add(criarItemInserirLista());

        return menu;
    }

    private JMenuItem criarItemGerarListaAleatória1() {
        JMenuItem menu = new JMenuItem ("Gerar Lista Aleatória");
        menu.setMnemonic(KeyEvent.VK_G);
//        menu.add(criarItemInserirNumeros());
        menu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ListaAleatoria1 l = new ListaAleatoria1(Janela.this);
            }
        });
        return menu;
        
    }
    private JMenuItem criarItemGerarListaAleatória2() {
        JMenuItem menu = new JMenuItem ("Gerar Lista Aleatória");
        menu.setMnemonic(KeyEvent.VK_G);
//        menu.add(criarItemInserirNumeros());
        menu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ListaAleatoria2 l = new ListaAleatoria2(Janela.this);
            }
        });
        return menu;
        
    }
//    
//    private JMenu criarSubMenuListar() {
//        JMenu menu = new JMenu("Listar");
//        menu.setMnemonic(KeyEvent.VK_L);
//
//        menu.add(criarItemTitulares());
//        menu.add(criarItemSaldos());
//
//        return menu;
//    }

//    private JMenuItem criarItemInserirLista() {
//             JMenuItem menu = new JMenuItem ("Inserir Lista");
//        menu.setMnemonic(KeyEvent.VK_I);
////        menu.add(criarItemN());
////        menu.add(criarItemQuantidadeListas());
//             menu.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//               Lista l = new Lista(Janela.this);
//            }
//        });
//        return menu;
//       
//    }
}


